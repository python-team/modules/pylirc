pylirc (0.0.5-4) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/watch: Use https protocol
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:02:49 -0500

pylirc (0.0.5-3) unstable; urgency=low

  * Added "--with python2" to debian/rules.
  * Updated package to source format 3.0 quilt.
  * Updated debian/copyright to DEP5.
  * debian/control
    - increased min python version to 2.6.6-3~.
    - removed dependency on python-support
    - changed XS-P-V to X-P-V.
    - increased Standards Version to 3.9.2 no changes needed.

 -- Charlie Smotherman <cjsmo@cableone.net>  Tue, 14 Jun 2011 15:22:45 -0500

pylirc (0.0.5-2) unstable; urgency=low

  * debian/control
    - Added myself to uploaders field.
    - Updated to use dh7 (Closes: #547837).
    - Removed python-dev.
    - Changed from python-central to python-support.
    - Removed XB-Python-Version.
    - Added Homepage field.
    - After consulting Arnaud moved package to DMPT for team maintenance.
    - Added DPMT to Uploaders field.
  * Removed debian/python-pylirc.install.
  * Increased debian/compat to 7.
  * Simplified debian/rules.
  * Added debian/preinst.
  * debian/copyright removed (C) and used the word copyright.
  * debian/copyright corrected link location for LGPL-2.

 -- Charlie Smotherman <cjsmo@cableone.net>  Sun, 20 Sep 2009 00:26:23 -0500

pylirc (0.0.5-1.1) unstable; urgency=low

  * NMU acknowledged by Arnaud Quette.
  * Move files to /usr/share/pyshared. Closes: #490520.

 -- Matthias Klose <doko@debian.org>  Sun, 13 Jul 2008 02:31:23 +0200

pylirc (0.0.5-1) unstable; urgency=low

  * Rename the source to pylirc, the binary to python-pylirc and reset
    changelog.
  * Rewrite build.
  * Drop useless debian/dirs.
  * Wrap build-deps and deps.
  * Add ${misc:Depends}.
  * Set lirc Maintainer Team as Maintainer and add Arnaud Quette and myself as
    Uploaders.
  * Rework descriptions.
  * Set Section to python.
  * Use ${shlibs:Depends} instead of depending on liblircclient0 manually.
  * Update copyright URLs.
  * Add watch file.
  * Initial upload to Debian; closes: #430642.

 -- Loic Minier <lool@dooz.org>  Wed, 25 Jul 2007 16:05:13 +0200
